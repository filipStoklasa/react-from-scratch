const path = require("path")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const WorkboxPlugin = require('workbox-webpack-plugin')
const { CleanWebpackPlugin } = require("clean-webpack-plugin")

module.exports = {
  mode: "production",
  devtool: "source-map",
  optimization: {
    nodeEnv: "production",
    minimize: true,
    minimizer: [ new UglifyJsPlugin() ],
    splitChunks: {
			cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/i,
          chunks: "all"
        },
				commons: {
					test: /[\\/]node_modules[\\/]/,
					name: 'commons',
          chunks: 'initial',
          minChunks: 2 
				}
			}
    },
    runtimeChunk: {
      name: "runtime"
    }
  },
  entry: {
    main:'./src/index.js'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: [ "@babel/env" ] }
      },
      {
        test: /\.s(a|c)ss$/,
        loader: [
          MiniCssExtractPlugin.loader,
          {
            loader: "css-loader",
            options: {
              modules: true,
            }
          },
          {
            loader: "sass-loader",
          },
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.(gif|png|jpg|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              webp: {
                quality: 75
              }
            },
          },
        ],
      },
    ]
  },
  resolve: { 
    extensions: [ "*", ".js", ".jsx", ".scss" ],
    modules: [ path.resolve("node_modules"), path.resolve("src") ]
  },
  output: {
    path: `${__dirname}/dist/`,
    filename: '[name].[chunkhash:8].js'
  },
  plugins: [
    new BundleAnalyzerPlugin(),
    new CleanWebpackPlugin(),
    new HtmlWebpackPlugin({
      template:  "./src/index.html",
      filename: "./index.html",
      inject: true,
      hash: false,
      minify: {
        removeComments:  true,
        collapseWhitespace:  true,
        minifyJS:  true,
        minifyCSS:  true
      }
    }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].[hash].css"
    }),
    new WorkboxPlugin.GenerateSW({
      exclude: [ /\.(?:png|jpg|jpeg|svg)$/ ],
      runtimeCaching: [ {
        urlPattern: /\.(?:png|jpg|jpeg|svg)$/,
        handler: 'CacheFirst',
        options: {
          cacheName: 'images',
          expiration: {
            maxEntries: 10,
          },
        },
      } ],
    })
  ]
}
