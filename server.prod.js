var path = require("path")
var express = require("express")
var compression = require('compression')
var DIST_DIR = path.join(__dirname, "dist")
var PORT = 3000
var app = express()

app.use(compression())

app.use(express.static(DIST_DIR))

app.get('/*', (req, res) => {
  res.sendFile(path.join(DIST_DIR, 'index.html'))
})

app.listen(PORT)