const path = require("path")
const webpack = require("webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {
	mode: 'development',
  devtool: 'source-map',
  entry: {
    index: [
      "webpack-hot-middleware/client",
      "./src/index.js",
		]
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: [ "@babel/env" ] }
      },
      {
        test: /\.s(a|c)ss$/,
        loader: [
          "style-loader",
          {
            loader: "css-loader",
            options: {
              modules: true,
              sourceMap: true
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: true
            }
          },
        ]
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      {
        test: /\.(gif|png|jpg|svg)$/i,
        use: [
          'file-loader',
          {
            loader: 'image-webpack-loader',
            options: {
              webp: {
                quality: 75
              }
            },
          },
        ],
      },
    ]
  },
  resolve: { 
    extensions: [ "*", ".js", ".jsx", ".scss" ],
    modules: [ path.resolve("node_modules"), path.resolve("src") ]
  },
  output: {
    path: path.resolve(__dirname, "local"),
    filename: "bundle.js",
    publicPath: "/",
    hotUpdateChunkFilename: ".hot/[id].[hash].hot-update.js",
    hotUpdateMainFilename: ".hot/[hash].hot-update.json"
},
  plugins: [
    new webpack.NamedModulesPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
        template:  "./src/index.html",
        filename: "./index.html",
      })
  ]
}
