module.exports = {
  "presets": [
    ["@babel/preset-env",
      {
      "modules": false,
        "targets": {
          "esmodules": true,
        },
      }
    ],
    "@babel/preset-react"
  ],
  "plugins": [
    "@babel/plugin-transform-runtime",
    "@babel/plugin-proposal-class-properties",
    "syntax-dynamic-import",
  ]
}