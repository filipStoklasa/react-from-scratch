import React from "react"
import { Provider } from 'react-redux'
import { Main } from './screens/main'
import { Help } from './screens/help'
import { createStore, compose, applyMiddleware  } from 'redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import reducers from 'store'
import rootSaga from 'store/saga'
import createSagaMiddleware from 'redux-saga'
import 'global.scss'
import { composeWithDevTools } from 'redux-devtools-extension'

const sagaMiddleware = createSagaMiddleware()

let middlewares = applyMiddleware(sagaMiddleware)

const composeEnhancer = process.env.NODE_ENV === 'development' ? composeWithDevTools : compose
const store = createStore(
  reducers,
  composeEnhancer(middlewares)
)

sagaMiddleware.run(rootSaga)

const App = () =>{
  return (  
    <Provider store={store}>
      <Router>
        <Route exact path="/" component={Main} />
        <Route path="/help" component={Help} />
      </Router>
    </Provider>
  )
}

export default App
