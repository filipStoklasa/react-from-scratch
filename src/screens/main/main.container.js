import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'

import { addToCounter } from 'store/state/counter'
import { callSpecimen } from 'store/state/specimen'
import css from './main.styles.scss'
class MainContainer extends PureComponent {
	addCounter = (event) => {
		event.stopPropagation()
		const { addToCounter } = this.props
		addToCounter && addToCounter()
	}

	fetchData = (event) => {
		event.stopPropagation()
		const { callSpecimen } = this.props
		callSpecimen && callSpecimen({ start:0, end:10 })
	}

  render(){
		const { counterS, data } = this.props
		return (
			<div className={css.box}>
				<NavLink exact to='/help'>
					<span>Help</span>
				</NavLink>
				<p>{JSON.stringify(counterS)}</p>
				<button onClick={this.addCounter}>ADD more</button>
				<button onClick={this.fetchData}>CALL</button>
				<pre>{JSON.stringify(data, null, 4)}</pre>
			</div>
		)
  }
}

const mapStateToProps = (state) => ({
  counterS: state.counter.counterStatus,
  data: state.specimen.data,
})

const mapDispatch = {
  addToCounter,
  callSpecimen
}


export const Main = connect(mapStateToProps, mapDispatch)(MainContainer)