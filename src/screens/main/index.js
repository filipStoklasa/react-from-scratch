import React, { Suspense, lazy } from 'react'

const MainComponent = lazy(() => import('./main.container' /* webpackChunkName: "mainContainer" */ ).then(module => ({ default: module.Main })))

export const Main = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <MainComponent />
    </Suspense>
  )
}