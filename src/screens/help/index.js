import React, { Suspense, lazy } from 'react'

const HelpComponent = lazy(() => import('./help.container' /* webpackChunkName: "helpContainer" */ ).then(module => ({ default: module.Help })))

export const Help = () => {
  return (
    <Suspense fallback={<div>Loading...</div>}>
      <HelpComponent />
    </Suspense>
  )
}