export const callSpecimen = (data) => ({ type: "CALL_SPECIMEN", payload: data })

export const reciveSpecimen = (data) => ({ type:"RECIVED_SPECIMEN", payload: data })

export const specimen = (state = {}, action) => {
  const newState = { ...state }
  switch (action.type) {
		case "RECIVED_SPECIMEN":
      newState.data = action.payload
      return newState
    default :
      return newState
  }
}