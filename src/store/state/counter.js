export const addToCounter = () => ({ type: "INCREMENT_COUNTER" })

const initialState = {
  counterStatus: 0
}
export const counter = (state = initialState, action) => {
  const newState = { ...state }
  switch (action.type) {
    case "INCREMENT_COUNTER":
      newState.counterStatus++
      return newState
    default :
      return newState
  }
}