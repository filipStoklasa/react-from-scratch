import { combineReducers } from 'redux'
import { counter } from './state/counter'
import { specimen } from './state/specimen'

export default combineReducers({
  counter,
  specimen
})