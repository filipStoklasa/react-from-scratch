import { reciveSpecimen } from '../state/specimen'
import { takeLatest, call, put } from 'redux-saga/effects'

 const getData = async ({ start, end }) => {
  return await fetch(`https://jsonplaceholder.typicode.com/todos?_start=${start}&_end=${end}`)
}

function* callSpecimenSaga({ payload }) {
  try {
    const response  = yield call(getData, payload)
    const data = yield response.json()
    yield put(reciveSpecimen(data))
  } catch (e) {
    alert(e)
  }
}

export function* specimenWatcher() {
  yield takeLatest('CALL_SPECIMEN', callSpecimenSaga)
}