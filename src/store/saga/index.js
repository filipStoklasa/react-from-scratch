import { all } from 'redux-saga/effects'
import { specimenWatcher } from './specimen'

export default function* rootSaga() {
  yield all([
    specimenWatcher()
  ])
}