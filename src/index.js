import React from "react"
import ReactDOM from "react-dom"
import App from "App.js"

if (process.env.NODE_ENV !== 'production') {
  if (module.hot) {
    module.hot.accept()
  }
}

ReactDOM.render(<App />, document.getElementById("root"))
