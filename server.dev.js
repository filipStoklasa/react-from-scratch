const path = require("path")
const express = require("express")
const PORT = 3000
const DIST_DIR = path.join(__dirname, "local")
const app = express()
const webpack = require("webpack")
const webpackConfig = require("./webpack.dev")
const compiler = webpack(webpackConfig)

app.use(
  require("webpack-dev-middleware")(compiler, {
    noInfo: true,
    publicPath: webpackConfig.output.publicPath
  })
)

app.use(require("webpack-hot-middleware")(compiler))
app.use(express.static("local"))

app.get('*', (req, res) => {
  res.sendFile(path.resolve(DIST_DIR, "index.html"))
})

app.listen(PORT)
